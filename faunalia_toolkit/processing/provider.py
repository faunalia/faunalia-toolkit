#! python3  # noqa: E265

"""
    Processing provider module.
"""

# PyQGIS
from qgis.core import QgsProcessingProvider
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

# project
from faunalia_toolkit.__about__ import DIR_PLUGIN_ROOT, __version__

from .algorithms.antipodes_from_layer import AntipodesFromPointLayer
from .algorithms.antipodes_from_point import AntipodesFromPoint
from .algorithms.copernicus import CopernicusAlgorithm
from .algorithms.open_meteo_weather_current import OpenMeteoWeatherCurrent
from .algorithms.open_meteo_weather_historical import OpenMeteoWeatherHistorical
from .algorithms.point_statistics_within_polygon import PointStatisticsWithinPolygon
from .algorithms.qpandas_reshape import QPandasReshape

# ############################################################################
# ########## Classes ###############
# ##################################


class FaunaliaToolkitProvider(QgsProcessingProvider):
    """
        Processing provider class.
    """

    def loadAlgorithms(self):
        """Loads all algorithms belonging to this provider."""
        self.addAlgorithm(AntipodesFromPoint())
        self.addAlgorithm(AntipodesFromPointLayer())
        self.addAlgorithm(CopernicusAlgorithm())
        self.addAlgorithm(OpenMeteoWeatherCurrent())
        self.addAlgorithm(OpenMeteoWeatherHistorical())
        self.addAlgorithm(PointStatisticsWithinPolygon())
        self.addAlgorithm(QPandasReshape())

    def id(self) -> str:
        """Unique provider id, used for identifying it. This string should be unique, \
        short, character only string, eg "qgis" or "gdal". \
        This string should not be localised.

        :return: provider ID
        :rtype: str
        """
        return "faunalia_toolkit"

    def name(self) -> str:
        """Returns the provider name, which is used to describe the provider
        within the GUI. This string should be short (e.g. "Lastools") and localised.

        :return: provider name
        :rtype: str
        """
        return self.tr("Faunalia Toolkit")

    def longName(self) -> str:
        """Longer version of the provider name, which can include
        extra details such as version numbers. E.g. "Lastools LIDAR tools". This string should be localised. The default
        implementation returns the same string as name().

        :return: provider long name
        :rtype: str
        """
        return self.tr("Faunalia Toolkit - Tools")

    def icon(self) -> QIcon:
        """QIcon used for your provider inside the Processing toolbox menu.

        :return: provider icon
        :rtype: QIcon
        """
        return QIcon(str(DIR_PLUGIN_ROOT / "resources/images/faunalia.svg"))

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: str
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate(self.__class__.__name__, message)

    def versionInfo(self) -> str:
        """Version information for the provider, or an empty string if this is not \
        applicable (e.g. for inbuilt Processing providers). For plugin based providers, \
        this should return the plugin’s version identifier.

        :return: version
        :rtype: str
        """
        return __version__
