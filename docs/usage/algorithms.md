# Algorithms

Description of all the algorithms of the plugin divided into categories.

## Cartography

### Antipodes (from layer)

Creates the antipode of the chosen point layer.

Conversion of the coordinates map are handled automatically and the final final
layer is in EPSG:4326.

Check [https://www.antipodesmap.com/](https://www.antipodesmap.com/) for more
curiosities.

Inspired by Flat Earth Society

### Antipodes (from point)

Creates the antipode from a selected point clicked on the map.

Conversion of the coordinates clicked on the map is handled automatically and
the final final layer is in EPSG:4326.

Check [https://www.antipodesmap.com/](https://www.antipodesmap.com/) for more
curiosities.

Inspired by Flat Earth Society

## Copernicus

### Download Copernicus Data

ERA5-Land hourly data from 1950 to present

This algorithm requires the additional **cdsapi** python library and an API key.
Please follow the [documentation](https://cds.climate.copernicus.eu/api-how-to)
to install the library and get the API Key. See also [cdsapi and API
key](cdsapi_installation).

Thanks to the cdsapi of the ERA5 project of Copernicus you can download dataset
from 1950 to (almost) today. The algorithm is just a simple GUI that uses the
cdsapi and fires the request.

Some checks are performed from the plugin itself (date checking) but the
ownership of the whole process is of ERA5 - Copernicus. To have a complete
documentation of all the variables please visit the copernicus website.

Muñoz Sabater, J., (2019): ERA5-Land hourly data from 1981 to present.
Copernicus Climate Change Service (C3S) Climate Data Store (CDS). (Accessed on
), 10.24381/cds.e2161bac

Muñoz Sabater, J., (2021): ERA5-Land hourly data from 1950 to 1980. Copernicus
Climate Change Service (C3S) Climate Data Store (CDS). (Accessed on ),
10.24381/cds.e2161bac

## Meteo

### Download Historical Open Meteo Data (Daily)

Download historicaldaily weather data from a location (chosen by coordinates).

The service is providen by
[https://open-meteo.com/en](https://open-meteo.com/en).

The resolution is ~2km.

Please check the
[documentation](https://open-meteo.com/en/docs/historical-weather-api) for
variable information.

No API key are required for non commercial use.

The algorithm outputs the html as the plot of the variables chosen with a red
dotted line representing today, a point layer of the clicked location and
optionally the whole JSON file with all the fetched values.

Weather data by [Open-Meteo.com](Open-Meteo.com)

### Weather Forecast (Hourly)

Download hourly weather data from a location (chosen by coordinates). Up to 16
days of forecast are possible.

The service is providen by
[https://open-meteo.com/en](https://open-meteo.com/en).

The resolution is ~2km.

Please check the [documentation](https://open-meteo.com/en/docs) for variable
information of click the Help button.

No API key are required for non commercial use.

The algorithm outputs the html as the plot of the variables chosen with a red
dotted line representing today, a point layer of the clicked location and
optionally the whole JSON file with all the fetched values.

Weather data by [Open-Meteo.com](Open-Meteo.com)

## Pandas

### Reshape Table (From Wide to Long)

Transforms a wide table to a long table. Choose one field as a category and a
set of fields that you want to reshape.

Is uses the [pandas
melt](https://pandas.pydata.org/docs/reference/api/pandas.melt.html) function.

Choosing "A" as the *id* field and "B" and "C" as the *value*
fields, the structure will change from this:

| A | B | C |
|---|---|---|
| a | 1 | 2 |
| b | 3 | 4 |
| c | 5 | 6 |

to this:

| A | variable | value |
|---|----------|-------|
| a | B        | 1     |
| b | B        | 3     |
| c | B        | 5     |
| a | C        | 2     |
| b | C        | 4     |
| c | C        | 6     |

## Vector Analysis

### Point Statistics Within Polygon

Given a point and a polygon layer, the algorithm takes only the points within
the polygon and calculates the statistics of the chosen fields. If not fields
are selected, all fields are taken into account.

The statistics are added as fields to the output polygon layer (e.g.
field_statistic)

The statistics strongly depends on the field type (numeric, text, date) and are
the standard statistics of QGIS (min, max, mean, median, count, count missing,
count distinct, variety, sum, range, iqr, st dev, minority, majority, first
quartile, third quartileinter quartile range).
