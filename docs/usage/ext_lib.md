# Copernicus

(cdsapi_installation)=
## `cdsapi` installation

Installation of the `cdsapi` python package.

*Windows*: open the `OSGEO4W Shell` and type:

    py3_env

and then:

    pip install cdsapi

*Linux* and *OSX* just open a terminal and type:

    pip install cdsapi

You might have to restart QGIS.

## API key

After the installation of the `cdsapi` package you also have to register to the
Copernicus portal to obtain an API key.

Please follow the instruction of the [official
documentation](https://cds.climate.copernicus.eu/api-how-to#use-the-cds-api-client-for-data-access)
to know how to get one.
