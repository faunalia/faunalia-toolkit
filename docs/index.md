# {{ title }} - Documentation

**Description:** {{ description }}

**Author and contributors:** {{ author }}

**Plugin version:** {{ version }}

**QGIS minimum version:** {{ qgis_version_min }}

**QGIS maximum version:** {{ qgis_version_max }}

**Source code:** {{ repo_url }}

**Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: Usage
maxdepth: 1
---
Algorithms <usage/algorithms>
External Modules <usage/ext_lib>
```
