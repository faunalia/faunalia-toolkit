# Faunalia Toolkit - QGIS Plugin

[![pipeline status](https://gitlab.com/faunalia/faunalia-toolkit/badges/main/pipeline.svg)](https://gitlab.com/faunalia/faunalia-toolkit/commits/main)

Cartographic and spatial awesome analysis tool and much much more!

The plugin adds many algorithms to the Processing toolbox that allows you to
perform some spatial analysis, download metereological data and weather
forecast.

You can find the official documentation
[here](https://faunalia.gitlab.io/faunalia-toolkit/)

## License

Distributed under the terms of the [`GPLv3` license](LICENSE).
